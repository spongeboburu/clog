/**
 * clog - A small, simplistic logging library for C99.
 * Copyright 2022 Peter Gebauer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "clog.h"
#include <string.h>
#include <stdlib.h>

#define CLOG_FORMAT_TIMESTAMP "%0.6f "
#define CLOG_FORMAT_CATEGORY "{%s} "
#define CLOG_FORMAT_FILENAME "(%s)"
#define CLOG_FORMAT_FILENAME_LN "(%s:%d)"

#define CLOG_SNPRINTF(s, n, l, f, ...) (l < n ? snprintf(s + l, n - l, f, __VA_ARGS__) : snprintf(NULL, 0, f, __VA_ARGS__))
#define CLOG_VSNPRINTF(s, n, l, f, a) (l < n ? vsnprintf(s + l, n - l, f, a) : vsnprintf(NULL, 0, f, a))

#define CLOG_ANSI_RESET "\e[0m"
#define CLOG_ANSI_DEBUG "\e[0;37m"
#define CLOG_ANSI_INFO "\e[1;97m"
#define CLOG_ANSI_WARNING "\e[1;93m"
#define CLOG_ANSI_ERROR "\e[1;91m"
#define CLOG_ANSI_CRITICAL "\e[0;101m\e[1;97m"
#define CLOG_ANSI_NONE "\e[0;107m\e[1;30m"

#ifdef _WIN32
#else
#include <time.h>
#endif

static const char *CLOG_LEVEL_NAMES_WITH_SPACES_[] = {
    "          ",
    "[CRITICAL]",
    "[ERROR]   ",
    "[WARNING] ",
    "[INFO]    ",
    "[DEBUG]   ",
    "[VERBOSE] ",
};

static double clog_get_time_(void)
{
#ifdef _WIN32
#else
    struct timespec tp;
    if (clock_gettime(CLOCK_MONOTONIC, &tp) == 0)
        return tp.tv_nsec / 1000000000.0 + tp.tv_sec;
#endif
    return 0.0;
}

static double clog_get_timestamp_(const struct clog *log)
{
    return clog_get_time_() - log->start_time;
}

void clog_init(
    struct clog *log,
    int level,
    void *user_data
)
{
    memset(log, 0, sizeof(struct clog));
    log->output = stdout;
    log->level = level;
    log->flags = CLOG_FLAG_TIMESTAMP | CLOG_FLAG_FILENAME | CLOG_FLAG_COLOR;
    for (int i = 0; i < CLOG_NUM_CATEGORIES; i++)
        log->categories[i].level = CLOG_LEVEL_VERBOSE;
    log->start_time = clog_get_time_();
}


void clog_deinit(
    struct clog *log
)
{
    if (log->close && log->output != NULL)
        fclose(log->output);
}

int clog_get_level(
    const struct clog *log
)
{
    return log->level;
}

void clog_set_level(
    struct clog *log,
    int level
)
{
    log->level = level;
}

void *clog_get_user_data(
    const struct clog *log
)
{
    return log->user_data;
}

void *clog_set_user_data(
    struct clog *log,
    void *user_data
)
{
    log->user_data = user_data;
}

unsigned int clog_get_flags(
    const struct clog *log
)
{
    return log->flags;
}

void clog_set_flags(
    struct clog *log,
    unsigned int flags
)
{
    log->flags = flags;
}

FILE *clog_get_output(
    const struct clog *log
)
{
    return log->output;
}

void clog_set_output(
    struct clog *log,
    FILE *output
)
{
    if (log->close && log->output)
        fclose(log->output);
    log->output = output;
    log->close = false;
}


void clog_set_output_stdout(
    struct clog *log
)
{
    clog_set_output(log, stdout);
}

void clog_set_output_stderr(
    struct clog *log
)
{
    clog_set_output(log, stderr);
}

int clog_set_output_file(
    struct clog *log,
    const char *filename,
    size_t n
)
{
    size_t i;
    char path[4096];
    FILE *f;
    for (i = 0; i < n && i < 4095 && filename[i] != 0; i++)
        path[i] = filename[i];
    path[i] = 0;
    f = fopen(path, "w");
    if (f == NULL)
        return -1;
    clog_set_output(log, f);
    log->close = true;
    return 0;
}

bool clog_get_close_output(
    const struct clog *log
)
{
    return log->close;
}

void clog_set_close_output(
    struct clog *log,
    bool close
)
{
    log->close = close;
}

clog_callback clog_get_callback(
    const struct clog *log
)
{
    return log->callback;
}

void clog_set_callback(
    struct clog *log,
    clog_callback callback
)
{
    log->callback = callback;
}

const char *clog_get_category_name(
    const struct clog *log,
    int category
)
{
    if (category >= 0 && category < CLOG_NUM_CATEGORIES
        && log->categories[category].name != NULL)
        return log->categories[category].name;
    return "";
}

int clog_set_category_name(
    struct clog *log,
    int category,
    const char *name,
    size_t n
)
{
    if (category >= 0 && category < CLOG_NUM_CATEGORIES)
    {
        size_t len = 0;
        while (len < n && len < sizeof(log->categories[category].name) - 1 && name[len] != 0)
        {
            log->categories[category].name[len] = name[len];
            len++;
        }
        log->categories[category].name[len] = 0;
        return 0;
    }
    return -1;
}

int clog_get_category_level(
    const struct clog *log,
    int category
)
{
    if (category >= 0 && category < CLOG_NUM_CATEGORIES)
        return log->categories[category].level;
    return -1;
}

int clog_set_category_level(
    struct clog *log,
    int category,
    int level
)
{
    if (category >= 0 && category < CLOG_NUM_CATEGORIES)
    {
        log->categories[category].level = level;
        return 0;
    }
    return -1;
}

int clog_set_category(
    struct clog *log,
    int category,
    const char *name,
    size_t n,
    int level
)
{
    clog_set_category_level(log, category, level);
    return clog_set_category_name(log, category, name, n);
}

size_t clog_format_va(
    const struct clog *log,
    char *s,
    size_t n,
    int category,
    int level,
    const char *filename,
    int line_number,
    const char *format,
    va_list args
)
{
    size_t len;
    va_list copy;
    const char *category_name = clog_get_category_name(log, category);
    const char *level_name = (level >= CLOG_LEVEL_NONE && level <= CLOG_LEVEL_VERBOSE ? CLOG_LEVEL_NAMES_WITH_SPACES_[level] : "");

    len = 0;
    if (log->flags & CLOG_FLAG_COLOR)
    {
        switch (level)
        {
        case CLOG_LEVEL_DEBUG:
        case CLOG_LEVEL_VERBOSE:
            len += CLOG_SNPRINTF(s, n, len, "%s", CLOG_ANSI_DEBUG);
            break;
        case CLOG_LEVEL_INFO:
            len += CLOG_SNPRINTF(s, n, len, "%s", CLOG_ANSI_INFO);
            break;
        case CLOG_LEVEL_WARNING:
            len += CLOG_SNPRINTF(s, n, len, "%s", CLOG_ANSI_WARNING);
            break;
        case CLOG_LEVEL_ERROR:
            len += CLOG_SNPRINTF(s, n, len, "%s", CLOG_ANSI_ERROR);
            break;
        case CLOG_LEVEL_CRITICAL:
            len += CLOG_SNPRINTF(s, n, len, "%s", CLOG_ANSI_CRITICAL);
            break;
        case CLOG_LEVEL_NONE:
            len += CLOG_SNPRINTF(s, n, len, "%s", CLOG_ANSI_NONE);
            break;
        default:
            len += CLOG_SNPRINTF(s, n, len, "%s", CLOG_ANSI_RESET);
            break;
        }
    }
    if (log->flags & CLOG_FLAG_TIMESTAMP)
        len += CLOG_SNPRINTF(s, n, len, CLOG_FORMAT_TIMESTAMP, clog_get_timestamp_(log));
    if (category_name[0] != 0)
        len += CLOG_SNPRINTF(s, n, len, CLOG_FORMAT_CATEGORY, category_name);
    len += CLOG_SNPRINTF(s, n, len, "%s", level_name);
    if ((log->flags & CLOG_FLAG_FILENAME)
        && filename != NULL)
    {
#ifdef _WIN32
        char *tmp = strrchr(filename, '\\');
#else
        char *tmp = strrchr(filename, '/');
#endif
        if (tmp != NULL)
            filename = tmp + 1;
        if (len + 1 < n)
            s[len] = ' ';
        len++;
        if (line_number >= 0)
            len += CLOG_SNPRINTF(s, n, len, CLOG_FORMAT_FILENAME_LN, filename, line_number);
        else
            len += CLOG_SNPRINTF(s, n, len, CLOG_FORMAT_FILENAME, filename);
    }
    len += CLOG_SNPRINTF(s, n, len, "%s", ": ");
    len += CLOG_VSNPRINTF(s, n, len, format, args);
    if (log->flags & CLOG_FLAG_COLOR)
        len += CLOG_SNPRINTF(s, n, len, "%s", CLOG_ANSI_RESET);
    return len;
}

size_t clog_format(
    const struct clog *log,
    char *s,
    size_t n,
    int category,
    int level,
    const char *filename,
    int line_number,
    const char *format,
    ...
)
{
    va_list args;
    va_start(args, format);
    size_t ret = clog_format_va(log, s, n, category, level, filename, line_number, format, args);
    va_end(args);
    return ret;
}

int clog_log_va(
    const struct clog *log,
    int category,
    int level,
    const char *filename,
    int line_number,
    const char *format,
    va_list args
)
{
    if (category < 0 || category >= CLOG_NUM_CATEGORIES)
        category = 0;
    if (level <= log->level && level <= log->categories[category].level)
    {
        size_t len;
        char buffer[CLOG_BUFFER_SIZE];
        char *tmps = buffer;
        va_list copy;
        bool do_output = true;
        va_copy(copy, args);
        len = clog_format_va(log, tmps, sizeof(buffer), category, level, filename, line_number, format, copy);
        va_end(copy);
        if (len >= sizeof(buffer) && CLOG_DYNAMIC_MAX > 0)
        {
            len++;
            if (len > sizeof(buffer) + CLOG_DYNAMIC_MAX)
                len = sizeof(buffer) + CLOG_DYNAMIC_MAX;
            tmps = malloc(len);
            if (tmps == 0)
                return -1;
            len = clog_format_va(log, tmps, len, category, level, filename, line_number, format, args);
        }
        va_end(args);
        if (log->callback != NULL)
            do_output = log->callback(log, category, level, tmps, len);
        if (do_output && log->output != NULL)
        {
            if (len == 0 || tmps[len - 1] != '\n')
                fprintf(log->output, "%s\n", tmps);
            else
                fprintf(log->output, "%s", tmps);
        }
        if (tmps != buffer)
            free(tmps);
        return do_output && log->output != NULL;
    }
    return 0;
}


int clog_log(
    const struct clog *log,
    int category,
    int level,
    const char *filename,
    int line_number,
    const char *format,
    ...
)
{
    va_list args;
    va_start(args, format);
    size_t ret = clog_log_va(log, category, level, filename, line_number, format, args);
    va_end(args);
    return ret;
}

bool clog_get_flag(
    const struct clog *log,
    unsigned int flag
)
{
    return log->flags & flag;
}

void clog_set_flag(
    struct clog *log,
    unsigned int flag
)
{
    log->flags |= flag;
}

void clog_clear_flag(
    struct clog *log,
    unsigned int flag
)
{
    log->flags &= ~flag;
}

const char *clog_levelstr(
    int level
)
{
    switch (level)
    {
    case CLOG_LEVEL_CRITICAL:
        return "CRITICAL";
    case CLOG_LEVEL_ERROR:
        return "ERROR";
    case CLOG_LEVEL_WARNING:
        return "WARNING";
    case CLOG_LEVEL_INFO:
        return "INFO";
    case CLOG_LEVEL_DEBUG:
        return "DEBUG";
    case CLOG_LEVEL_VERBOSE:
        return "VERBOSE";
    }
    return "";
}
