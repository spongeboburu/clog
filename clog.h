/**
 * @file clog.h
 * @brief A small, simplistic logging library for C99.
 * @copyright Peter Gebauer 2022 (MIT license)
 *
 * @section log_levels Log levels
 *
 * - @ref CLOG_LEVEL_CRITICAL
 * - @ref CLOG_LEVEL_ERROR
 * - @ref CLOG_LEVEL_WARNING
 * - @ref CLOG_LEVEL_DEBUG
 * - @ref CLOG_LEVEL_VERBOSE
 *
 * @section log_flags Log flags
 *
 * These are used to control formatting.
 *
 * - @ref CLOG_FLAG_TIMESTAMP
 * - @ref CLOG_FLAG_FILENAME
 * - @ref CLOG_FLAG_COLOR
 *
 * @section build_options Build options
 *
 * At compile time the following definitions are available to change:
 *
 * - @ref CLOG_NUM_CATEGORIES The maximum number of categories (default: 32).
 * - @ref CLOG_BUFFER_SIZE The size of the static buffer (default: 8192).
 * - @ref CLOG_DYNAMIC_MAX The maximum size for dynamically allocated memory (default: 0).
 *
 *
 * @section license License
 *
 * ```unparsed
 * Copyright 2022 Peter Gebauer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * ```
 */

#ifndef CLOG_H
#define CLOG_H

/**
 * @cond
 */
#ifndef _POSIX_C_SOURCE
#define  _POSIX_C_SOURCE 199309L
#endif

/**
 * @endcond
 */

#include <stdio.h>
#include <stdbool.h>
#include <stdarg.h>

/**
 * @def CLOG_API
 * @brief External linkage visibility.
 */
#ifdef CLOG_BUILD_API
#ifdef _WIN32
#define CLOG_API  __declspec(dllexport)
#else
#define CLOG_API  __attribute__ ((visibility ("default")))
#endif
#else
#ifdef _WIN32
#define CLOG_API __declspec(dllimport)
#else
#define CLOG_API __attribute__ ((visibility ("hidden")))
#endif
#endif

/**
 * @def CLOG_NUM_CATEGORIES
 * @brief The maximum number of categories a log has.
 * @note This is a build time option.
 * If not defined at build time the default of 32 is used.
 */
#ifndef CLOG_NUM_CATEGORIES
#define CLOG_NUM_CATEGORIES 32
#endif

/**
 * @def CLOG_BUFFER_SIZE
 * @brief The size of the internal, automatic storage duration buffer.
 * @note This is a build time option.
 * If not defined at build time the default of 8192 is used.
 * @note The size of this buffer may be limited by a stack size
 * or the C implementation since the standard has various limits
 * and not compilers may refuse to compile oversized objects.
 */
#ifndef CLOG_BUFFER_SIZE
#define CLOG_BUFFER_SIZE 8192
#endif

/**
 * @def CLOG_DYNAMIC_MAX
 * @brief The maximum number of bytes to allocate dynamically
 * if a log message will not fit the buffer (@ref CLOG_BUFFER_SIZE).
 * @note This is a build time option.
 * If not defined at build time the default is 0 (no dynamic allocation
 * allowed).
 * @note This is the size in addition to @ref CLOG_BUFFER_SIZE.
 */
#ifndef CLOG_DYNAMIC_MAX
#define CLOG_DYNAMIC_MAX 0
#endif

/**
 * @def CLOG_PRINTF
 * @brief A compiler specific macro to check for printf formatting.
 * @param format The 1-based index of the format argument.
 * @param varargs The 1-based index of the varargs argument.
 * @note Only enabled for compilers known to support this attribute.
 */
#if defined(__GNUC__) || defined(__clang__)
#define CLOG_PRINTF(format, varargs) __attribute__ ((__format__ (__printf__, format, varargs)))
#else
#define CLOG_PRINTF(format, varargs)
#endif

/**
 * @brief Output messages that will always be shown.
 *
 * This can be used instead of using temporary printf debugging,
 * these messages will be highly visible.
 */
#define CLOG_LEVEL_NONE 0

/**
 * @brief Output critical messages.
 * @see @ref log_levels.
 *
 * These messages describe errors that are difficult or
 * even impossible to deal with to the point where terminating
 * the process might be the best option or that it may
 * enter a state instability or even undefined behaviour.
 *
 * This can be used to describe a reason for terminating
 * the process, a failure to allocate required resources
 * or even report bugs.
 */
#define CLOG_LEVEL_CRITICAL 1

/**
 * @brief Output critical and error messages.
 * @see @ref log_levels.
 *
 * These messages describe errors that will halt the current
 * operation, but have no lasting effect on the program.
 *
 * This can be failure to allocate the resources required to
 * continue the operation, invalid arguments, etc.
 */
#define CLOG_LEVEL_ERROR 2

/**
 * @brief Output critical, error and warning messages.
 * @see @ref log_levels.
 *
 * These messages describes a warning that the operation will continue,
 * but some features might be missing and the information is useful
 * to someone reading the log.
 */
#define CLOG_LEVEL_WARNING 3

/**
 * @brief Output critical, error, warning and information messages.
 * @see @ref log_levels.
 *
 * These messages are used for logging general information
 * about normal operations and what the program is doing.
 */
#define CLOG_LEVEL_INFO 4

/**
 * @brief Output critical, error, warning, information
 * and debug messages.
 * @see @ref log_levels.
 *
 * These messages are used for debugging purposes and should
 * contain information useful to developers of the program.
 */
#define CLOG_LEVEL_DEBUG 5

/**
 * @brief Output all messages.
 * @see @ref log_levels.
 *
 * These messages are meant to suplement debug messages
 * with extra information such as data dumps, but they
 * can be used as is to log the most verbose messages.
 */
#define CLOG_LEVEL_VERBOSE 6

/**
 * @brief Used to control if a timestamp should be included in the
 * message format.
 * @see @ref log_flags.
 */
#define CLOG_FLAG_TIMESTAMP 1

/**
 * @brief Used to control if the filename and line number should be
 * included in the message format.
 * @see @ref log_flags.
 */
#define CLOG_FLAG_FILENAME 2

/**
 * @brief Used to control if the message should include ANSI codes.
 * @see @ref log_flags.
 */
#define CLOG_FLAG_COLOR 4

/**
 * @brief Helper function to log a "none" message.
 * @param log The log.
 * @param category The category.
 * @param ... Variadic arguments, the first one is the format string (printf).
 * @returns 1 if the message was accepted, 0 if not or -1 if out of memory.
 */
#define CLOG_LOG_NONE(log, category, ...) clog_log(log, category, CLOG_LEVEL_NONE, __FILE__, __LINE__, __VA_ARGS__);

/**
 * @brief Helper function to log a critical message.
 * @param log The log.
 * @param category The category.
 * @param ... Variadic arguments, the first one is the format string (printf).
 * @returns 1 if the message was accepted, 0 if not or -1 if out of memory.
 */
#define CLOG_LOG_CRITICAL(log, category, ...) clog_log(log, category, CLOG_LEVEL_CRITICAL, __FILE__, __LINE__, __VA_ARGS__);

/**
 * @brief Helper function to log a error message.
 * @param log The log.
 * @param category The category.
 * @param ... Variadic arguments, the first one is the format string (printf).
 * @returns 1 if the message was accepted, 0 if not or -1 if out of memory.
 */
#define CLOG_LOG_ERROR(log, category, ...) clog_log(log, category, CLOG_LEVEL_ERROR, __FILE__, __LINE__, __VA_ARGS__);

/**
 * @brief Helper function to log a warning message.
 * @param log The log.
 * @param category The category.
 * @param ... Variadic arguments, the first one is the format string (printf).
 * @returns 1 if the message was accepted, 0 if not or -1 if out of memory.
 */
#define CLOG_LOG_WARNING(log, category, ...) clog_log(log, category, CLOG_LEVEL_WARNING, __FILE__, __LINE__, __VA_ARGS__);

/**
 * @brief Helper function to log a info message.
 * @param log The log.
 * @param category The category.
 * @param ... Variadic arguments, the first one is the format string (printf).
 * @returns 1 if the message was accepted, 0 if not or -1 if out of memory.
 */
#define CLOG_LOG_INFO(log, category, ...) clog_log(log, category, CLOG_LEVEL_INFO, __FILE__, __LINE__, __VA_ARGS__);

/**
 * @brief Helper function to log a debug message.
 * @param log The log.
 * @param category The category.
 * @param ... Variadic arguments, the first one is the format string (printf).
 * @returns 1 if the message was accepted, 0 if not or -1 if out of memory.
 */
#define CLOG_LOG_DEBUG(log, category, ...) clog_log(log, category, CLOG_LEVEL_DEBUG, __FILE__, __LINE__, __VA_ARGS__);

/**
 * @brief Helper function to log a verbose message.
 * @param log The log.
 * @param category The category.
 * @param ... Variadic arguments, the first one is the format string (printf).
 * @returns 1 if the message was accepted, 0 if not or -1 if out of memory.
 */
#define CLOG_LOG_VERBOSE(log, category, ...) clog_log(log, category, CLOG_LEVEL_VERBOSE, __FILE__, __LINE__, __VA_ARGS__);

struct clog;

/**
 * @brief A callback to handle a log message.
 * @param log The log that was issued a message.
 * @param category The index of the category of the message.
 * @param level The log level for the message.
 * @param s A pointer to the zero terminated message string.
 * @param n The length of the message excluding the zero terminator.
 * @returns True if the log should perform the normal output operations
 * or false to omit output.
 */
typedef bool (*clog_callback)(
    const struct clog *log,
    int category,
    int level,
    const char *s,
    size_t n
);

/**
 * @brief A log.
 */
struct clog
{

    /**
     * @brief The categories.
     */
    struct
    {

        /**
         * @brief The category level.
         */
        int level;

        /**
         * @brief The category name.
         */
        char name[32];

    } categories[CLOG_NUM_CATEGORIES];

    /**
     * @brief The output stream.
     */
    FILE *output;

    /**
     * @brief Intercept a message.
     */
    clog_callback callback;

    /**
     * @brief Arbitrary user data associated with the log.
     */
    void *user_data;

    /**
     * @brief Note the time when the log was initialized.
     */
    double start_time;

    /**
     * @brief The level used to filter messages.
     */
    int level;

    /**
     * @brief Flags used to control formatting.
     */
    unsigned int flags;

    /**
     * @brief If true the log will close the file on log deinit
     * or replaced by a new stream.
     */
    bool close;

};

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Initialize a log.
 * @param[out] log The log struct to initialize.
 * @param level The log level  (@ref log_levels).
 * @param user_data Arbitrary user data.
 *
 * The categories will all have their levels set to
 * @ref CLOG_LEVEL_VERBOSE.
 *
 * The output is set to stdout (see clog_set_output()).
 *
 * The format flags will be set to both @ref CLOG_FLAG_TIMESTAMP
 * and @ref CLOG_FLAG_FILENAME (see clog_set_flags()).
 */
CLOG_API void clog_init(
    struct clog *log,
    int level,
    void *user_data
);

/**
 * @brief Deinitialize a log.
 * @param[out] log The log struct to deinitialize.
 */
CLOG_API void clog_deinit(
    struct clog *log
);

/**
 * @brief Get the log level.
 * @param log The log.
 * @returns The log level.
 * @see clog_set_level().
 */
CLOG_API int clog_get_level(
    const struct clog *log
);

/**
 * @brief Set the log level.
 * @param log The log.
 * @param level The log level (@ref log_levels).
 * @see clog_get_level().
 */
CLOG_API void clog_set_level(
    struct clog *log,
    int level
);

/**
 * @brief Get the arbitrary user data pointer.
 * @param log The log.
 * @returns The arbitrary user data pointer.
 * @see clog_set_user_data().
 */
CLOG_API void *clog_get_user_data(
    const struct clog *log
);

/**
 * @brief Set the arbitrary user data pointer.
 * @param log The log.
 * @param user_data The new arbitrary user data.
 * @returns The previous user data pointer.
 * @see clog_get_user_data().
 */
CLOG_API void *clog_set_user_data(
    struct clog *log,
    void *user_data
);

/**
 * @brief Get the log flags.
 * @param log The log.
 * @returns The log flags.
 * @see clog_set_flags().
 */
CLOG_API unsigned int clog_get_flags(
    const struct clog *log
);

/**
 * @brief Set the log flags.
 * @param log The log.
 * @param flags The log flags (@ref log_flags).
 * @see clog_get_flags().
 */
CLOG_API void clog_set_flags(
    struct clog *log,
    unsigned int flags
);

/**
 * @brief Get the output stream for a log category.
 * @param log The log.
 * @returns A pointer to the stream, may be NULL if no output is used.
 * @see clog_set_output().
 */
CLOG_API FILE *clog_get_output(
    const struct clog *log
);

/**
 * @brief Set the output stream for a log.
 * @param[out] log The log.
 * @param output The output stream or NULL for none.
 * @note If an existing stream is about to be overwritten
 * and the close flag is set using clog_set_close_output()
 * it will be closed.
 * @note This will reset the close status flag to false,
 * call clog_set_close_output() to change it if wanted.
 * @see clog_get_output().
 */
CLOG_API void clog_set_output(
    struct clog *log,
    FILE *output
);

/**
 * @brief Set stdout as the output stream for a log.
 * @param[out] log The log.
 * @note If an existing stream is about to be overwritten
 * and the close flag is set using clog_set_close_output()
 * it will be closed.
 */
CLOG_API void clog_set_output_stdout(
    struct clog *log
);

/**
 * @brief Set stderr as the output stream for a log.
 * @param[out] log The log.
 * @note If an existing stream is about to be overwritten
 * and the close flag is set using clog_set_close_output()
 * it will be closed.
 */
CLOG_API void clog_set_output_stderr(
    struct clog *log
);

/**
 * @brief Open a file for writing and set it as the output
 * and set the close status flag to true.
 * @param[out] log The log.
 * @param filename The filename.
 * @param n Read at most this many bytes from @p filename.
 * @returns 0 on success or -1 if an error occurred opening
 * the file (errno will be set by the call to fopen).
 * @note If the @p filename is an existing file it will
 * be truncated.
 * @note If an existing stream is about to be overwritten
 * and the close flag is set using clog_set_close_output()
 * it will be closed.
 */
CLOG_API int clog_set_output_file(
    struct clog *log,
    const char *filename,
    size_t n
);

/**
 * @brief Get the status flag for file closing.
 * @param log The log.
 * @returns True if the output is to be closed if overwritten
 * or if the log is deinitialized.
 * @see clog_set_close_output().
 */
CLOG_API bool clog_get_close_output(
    const struct clog *log
);

/**
 * @brief Set the status of file closing.
 * @param log The log.
 * @param close True if the file should be closed when deinitializing
 * the log or if the output is overwritten.
 * @see clog_get_close_output().
 */
CLOG_API void clog_set_close_output(
    struct clog *log,
    bool close
);

/**
 * @brief Get the callback function.
 * @param log The log.
 * @returns The callback function or NULL if no callback is set.
 * @see clog_set_callback().
 */
CLOG_API clog_callback clog_get_callback(
    const struct clog *log
);

/**
 * @brief Set the callback function.
 * @param log The log.
 * @param callback The callback function or NULL for no callback.
 * @see clog_get_callback().
 */
CLOG_API void clog_set_callback(
    struct clog *log,
    clog_callback callback
);

/**
 * @brief Get the name for a category.
 * @param log The log.
 * @param category The index of the category.
 * @returns A pointer to the name or a pointer to an empty string
 * if @p category is invalid or the category has no name.
 * @see clog_set_category_name() and clog_set_category().
 */
CLOG_API const char *clog_get_category_name(
    const struct clog *log,
    int category
);

/**
 * @brief Set the name for a category.
 * @param log The log.
 * @param category The index of the category.
 * @param name The new name or NULL for no name.
 * @param n Read at most this many bytes from @p name.
 * @returns 0 on success or -1 if the @p category is out of bounds.
 * @note The maximum length of a category name is 32 characters
 * including the zero terminator.
 * @see clog_get_category_name() and clog_set_category().
 */
CLOG_API int clog_set_category_name(
    struct clog *log,
    int category,
    const char *name,
    size_t n
);

/**
 * @brief Get the level for a category.
 * @param log The log.
 * @param category The index of the category.
 * @returns The category level on success or -1 if @p category
 * is out of bounds.
 * @see clog_set_category_level() and clog_set_category().
 */
CLOG_API int clog_get_category_level(
    const struct clog *log,
    int category
);

/**
 * @brief Set the level for a category.
 * @param log The log.
 * @param category The index of the category.
 * @param level The log level (@ref log_levels).
 * @returns 0 on success or -1 if the @p category is out of bounds.
 * @note By default category levels are set to @ref CLOG_LEVEL_VERBOSE.
 * @see clog_get_category_level() and clog_set_category().
 */
CLOG_API int clog_set_category_level(
    struct clog *log,
    int category,
    int level
);

/**
 * @brief Set the name and level for a category.
 * @param log The log.
 * @param category The index of the category.
 * @param name The new name or NULL for no name.
 * @param n Read at most this many bytes from @p name.
 * @param level The log level (@ref log_levels).
 * @returns 0 on success or -1 if the @p category is out of bounds
 * or -2 if out of memory.
 * @see clog_set_category_name() and clog_set_category_level().
 */
CLOG_API int clog_set_category(
    struct clog *log,
    int category,
    const char *name,
    size_t n,
    int level
);

/**
 * @brief Format arguments as if a log message.
 * @param log The log.
 * @param[out] s Store the message in this string.
 * @param n Store at most this many bytes in @p s, including
 * any zero terminator. If @p n is 0 nothing is stored.
 * @param category The category index. If it is invalid category 0 is used.
 * @param level The log level (@ref log_levels). If it is invalid
 * @ref CLOG_LEVEL_VERBOSE is used.
 * @param filename A filename to include in the formatted string or
 * NULL for none.
 * @param line_number A number to include in the formatted string
 * or a negative value for none. Note that if @p filename is NULL
 * this argument is ignored.
 * @param format The format string.
 * @param args Variadic arguments for the @p format string.
 * @returns 1 if the message was accepted or 0 if not. If the function
 * encounters an out of memory error it will return -1.
 * @note The file name is truncated to max 128 characters, including
 * the zero terminator.
 * @note This function will not call set callback or output anything
 * to the output stream.
 * @see clog_format() and clog_log_va().
 */
CLOG_API size_t clog_format_va(
    const struct clog *log,
    char *s,
    size_t n,
    int category,
    int level,
    const char *filename,
    int line_number,
    const char *format,
    va_list args
);

/**
 * @brief Format arguments as if a log message.
 * @param log The log.
 * @param[out] s Store the message in this string.
 * @param n Store at most this many bytes in @p s, including
 * @param category The category index. If it is invalid category 0 is used.
 * @param level The log level (@ref log_levels). If it is invalid
 * @ref CLOG_LEVEL_VERBOSE is used.
 * @param filename A filename to include in the formatted string or
 * NULL for none.
 * @param line_number A number to include in the formatted string
 * or a negative value for none. Note that if @p filename is NULL
 * this argument is ignored.
 * @param format The format string.
 * @param ... Variadic arguments for the @p format string.
 * @returns The length of the message, excluding the zero terminator.
 * If the function encounters out of memory the function exits
 * early with 0.
 * @note The file name is truncated to max 128 characters, including
 * the zero terminator.
 * @note This function will not call set callback or output anything
 * to the output stream.
 * @see clog_format_va() and clog_log().
 */
CLOG_API size_t clog_format(
    const struct clog *log,
    char *s,
    size_t n,
    int category,
    int level,
    const char *filename,
    int line_number,
    const char *format,
    ...
)  CLOG_PRINTF(8, 9);

/**
 * @brief Log a message.
 * @param log The log.
 * @param category The category index. If it is invalid category 0 is used.
 * @param level The log level (@ref log_levels). If it is invalid
 * @ref CLOG_LEVEL_VERBOSE is used.
 * @param filename A filename to include in the formatted string or
 * NULL for none.
 * @param line_number A number to include in the formatted string
 * or a negative value for none. Note that if @p filename is NULL
 * this argument is ignored.
 * @param format The format string.
 * @param args Variadic arguments for the @p format string.
 * @returns 1 if the message was accepted, 0 if not or -1 if out of memory.
 * @note If a newline is not detected at the end one will be implicitly added
 * for any output.
 * @see clog_log() and clog_format_va().
 */
CLOG_API int clog_log_va(
    const struct clog *log,
    int category,
    int level,
    const char *filename,
    int line_number,
    const char *format,
    va_list args
);

/**
 * @brief Log a message.
 * @param log The log.
 * @param category The category index. If it is invalid category 0 is used.
 * @param level The log level (@ref log_levels). If it is invalid
 * @ref CLOG_LEVEL_VERBOSE is used.
 * @param filename A filename to include in the formatted string or
 * NULL for none.
 * @param line_number A number to include in the formatted string
 * or a negative value for none. Note that if @p filename is NULL
 * this argument is ignored.
 * @param format The format string.
 * @param ... Variadic arguments for the @p format string.
 * @returns 1 if the message was accepted, 0 if not or -1 if out of memory.
 * @note If a newline is not detected at the end one will be implicitly added
 * for any output.
 * @see clog_log_va() and clog_format().
 */
CLOG_API int clog_log(
    const struct clog *log,
    int category,
    int level,
    const char *filename,
    int line_number,
    const char *format,
    ...
) CLOG_PRINTF(6, 7);

/**
 * @brief Helper to get a flag.
 * @param log The log.
 * @param flag The flag to get.
 * @returns True if the flag is set, false if not.
 * @see clog_set_flag() and clog_clear_flag().
 */
CLOG_API bool clog_get_flag(
    const struct clog *log,
    unsigned int flag
);

/**
 * @brief Helper to set a flag.
 * @param log The log.
 * @param flag The flag to set.
 * @see clog_set_flag() and clog_clear_flag().
 */
CLOG_API void clog_set_flag(
    struct clog *log,
    unsigned int flag
);

/**
 * @brief Helper to clear a flag.
 * @param log The log.
 * @param flag The flag to clear.
 * @see clog_get_flag() and clog_set_flag().
 */
CLOG_API void clog_clear_flag(
    struct clog *log,
    unsigned int flag
);

/**
 * @brief Get a human readable, all caps name of a log level.
 * @param level The level.
 * @returns A pointer to the name string or a pointer to an empty
 * string if @p level is invalid.
 */
CLOG_API const char *clog_levelstr(
    int level
);

#ifdef __cplusplus
}
#endif

#endif
