/**
 * @example example.c
 * Part of clog - A small, simplistic logging library for C99.
 * Copyright 2022 Peter Gebauer
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files
 * (the "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "clog.h"
#include <string.h>

static bool test_callback(
    const struct clog *log,
    int category,
    int level,
    const char *s,
    size_t n
)
{
    // Do something here, maybe redirect to another system.
    // return false; // Do not output this message.
    return true; // Please continue with output.
}

static void print_help(void)
{
    printf("clog_example [options]\n");
    printf("-h  Show help.\n");
    printf("-t  Enable timestamp.\n");
    printf("-f  Enable filename.\n");
    printf("-c  Enable color.\n");
}

int main(int argc, char *argv[])
{
    unsigned int flags = 0;
    struct clog log;
    for (int i = 1; i < argc; i++)
    {
        if (strcmp(argv[i], "-h") == 0)
        {
            print_help();
            return 0;
        }
    }
    for (int i = 1; i < argc; i++)
    {
        if (strcmp(argv[i], "-t") == 0)
            flags |= CLOG_FLAG_TIMESTAMP;
        else if (strcmp(argv[i], "-f") == 0)
            flags |= CLOG_FLAG_FILENAME;
        else if (strcmp(argv[i], "-c") == 0)
            flags |= CLOG_FLAG_COLOR;
        else
        {
            printf("Invalid option: %s\n", argv[i]);
            return 1;
        }
    }

    clog_init(&log, CLOG_LEVEL_VERBOSE, NULL);
    clog_set_callback(&log, test_callback);
    clog_set_category_name(&log, 0, "testcat", 1024);
    clog_set_flags(&log, flags);
    CLOG_LOG_NONE(&log, 0, "Hello %d", 0);
    CLOG_LOG_CRITICAL(&log, 0, "Hello %d", 1);
    CLOG_LOG_ERROR(&log, 0, "Hello %d", 2);
    CLOG_LOG_WARNING(&log, 0, "Hello %d", 3);
    CLOG_LOG_INFO(&log, 0, "Hello %d", 4);
    CLOG_LOG_DEBUG(&log, 0, "Hello %d", 5);
    CLOG_LOG_VERBOSE(&log, 0, "Hello %d", 6);
    clog_deinit(&log);
    return 0;
}
