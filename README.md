# clog - A small, simplistic logging library for C99.

I just kept rewriting this code for every project where I needed to do simple
logging. This is nothing like a fully fledged logging system, but it does
provide the features needed to produce some colorized output to stdout, stderr,
a file or custom callback handling.

I don't intend to add much more to this since it's major goal is
being simple to setup and use.

Features:

- 32 categories with individual settings for log level.
    - Build time option.
- Log to any `FILE *`.
- Callback system to intercept and filter/redirect output.
- Optional ANSI colors.
- By default no dynamic memory allocations.
    - Build time option.
- 7 levels:
    - CLOG_LEVEL_NONE used for temporary output in the same way printf
      would be used for "advanced" debugging. :)
    - CLOG_LEVEL_CRITICAL
    - CLOG_LEVEL_ERROR
    - CLOG_LEVEL_WARNING
    - CLOG_LEVEL_INFO
    - CLOG_LEVEL_DEBUG
    - CLOG_LEVEL_VERBOSE extra verbose for those debugging intense moments.


## Minimal example

```c
#include "clog.h"

int main(int argc, char *argv[])
{
    struct clog log;
    clog_init(&log, CLOG_LEVEL_VERBOSE, NULL);
    clog_set_category_name(&log, 0, "testcat", 1024);
    clog_set_flags(&log, CLOG_FLAG_TIMESTAMP | CLOG_FLAG_FILENAME | CLOG_FLAG_COLOR);
    CLOG_LOG_NONE(&log, 0, "Hello %d", 0);
    CLOG_LOG_CRITICAL(&log, 0, "Hello %d", 1);
    CLOG_LOG_ERROR(&log, 0, "Hello %d", 2);
    CLOG_LOG_WARNING(&log, 0, "Hello %d", 3);
    CLOG_LOG_INFO(&log, 0, "Hello %d", 4);
    CLOG_LOG_DEBUG(&log, 0, "Hello %d", 5);
    CLOG_LOG_VERBOSE(&log, 0, "Hello %d", 6);
    clog_deinit(&log);
    return 0;
}
```

![Screenshot](screenshot.png)


## Building

Requirements:

- A C99 compliant compiler.
- C standard libraries **stdlib.h**, **stdio.h**, **stdbool.h**, **stdarg.h** and **string.h**.
- cmake (optional)

Using cmake:

```sh
mkdir clog/build
cd clog/build
cmake ..
make
```

All the build time options can be changed via cmake.

Just using a compiler (for example gcc on Linux):

```sh
cd clog
cc -std=c99 -shared -fpic clog.c 
```

On non-Linux `-fpic` is omitted.


## License

Copyright 2022 Peter Gebauer

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files
(the "Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
